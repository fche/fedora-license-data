# This file is for collecting the actual text found in Fedora
# packages that have used the Callaway short name, "Public Domain" 
# or the SPDX expression, "LicenseRef-Fedora-Public-Domain"
# See the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_callaway_short_name_categories
# 
# This file is for collecting the text of such public domain dedications.
#
# Please search this file first to see if the public domain text you've found 
# is the same as text already recorded here. If so, simply add  
# your package name and location to the existing part of this file that contains that text.
#
# Include the following information:
# 
# Fedora package name
#
# Location of where you found the public domain dedication text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text of the public dedication found that corresponds to the use of 
# the "Public Domain" (previously) or "LicenseRef-Fedora-Public-Domain" SPDX id.
# Remove blank lines.
#
# Copy template below and add yours to top of list, adding a space between entries.

package = 
location = 
text = '''
text here
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man2/nfsservctl.2#L2
text = '''
This text is in the public domain.
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man3/getnameinfo.3#L2
           https://github.com/mkerrisk/man-pages/blob/master/man8/tzselect.8#L2
text = '''
This page is in the public domain.
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man3/grantpt.3#L2
text = '''
This page is in the public domain. - aeb
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man3/stdin.3#L7
text = '''
Placed in the Public Domain.
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man8/ld.so.8#L2
text = '''
This is in the public domain
'''

package = automake
location = https://git.savannah.gnu.org/cgit/automake.git/tree/lib/mkinstalldirs?h=v1.16#n8
text = '''
Public domain.
'''

package = words
location = https://web.archive.org/web/20170930060409/http://icon.shef.ac.uk/Moby/
text = '''
The Moby lexicon project is complete and has
been place into the public domain. Use, sell,
rework, excerpt and use in any way on any platform.

Placing this material on internal or public servers is
also encouraged. The compiler is not aware of any
export restrictions so freely distribute world-wide.

You can verify the public domain status by contacting

Grady Ward
3449 Martha Ct.
Arcata, CA  95521-4884

daedal@myrealbox.com
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/libinstpatch/md5.c#L2
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/examples/create_sf2.c#L11
text = '''
Use this example as you please (public domain)
'''

package = libinstpatch
location = https://github.com/swami/libinstpatch/blob/v1.1.6/examples/split_sfont.c#L6
text = '''
Public domain use as you please
'''

package = perl
location = https://github.com/Perl/perl5/blob/blead/ext/SDBM_File/sdbm.h#L4-L5
text = '''
 * author: oz@nexus.yorku.ca
 * status: public domain. 
'''

package = perl
          perl-Test-Simple
location = https://github.com/Test-More/test-more/blob/master/lib/Test/Tutorial.pod
text = '''
Irrespective of its distribution, all code examples in these files
are hereby placed into the public domain.  You are permitted and
encouraged to use this code in your own programs for fun
or for profit as you see fit.  A simple comment in the code giving
credit would be courteous but is not required.
'''

package = python-multiprocess
location = https://github.com/uqfoundation/multiprocess/blob/273ee14261c9e08d794ab55f3398926e04a77364/py3.11/doc/html4css1.css#L6
text = '''
This stylesheet has been placed in the public domain.
'''

package = python-llvmlite
location = https://github.com/numba/llvmlite/blob/v0.39.1/versioneer.py#L278-L280
text = '''
To make Versioneer easier to embed, all its code is hereby released into the
public domain. The `_version.py` that it creates is also in the public
domain.
'''

package = perl-Barcode-Code128
location = https://metacpan.org/release/WRW/Barcode-Code128-2.21/source/README#L50
text = '''
This module is placed into the public domain.  You are free to
use, modify, or redistribute this module in any way for commercial
or other uses.  My only request is that if you change it, please
submit copies of your changed code (or diffs) so that I can
incorporate them into the version on CPAN.  Also, in order to
reduce the likelihood of confusion please do not distribute a
modified version of this module unless you change the name first.
'''

package = perl-libxml-perl
location = https://metacpan.org/release/KMACLEOD/libxml-perl-0.08/source/lib/XML/Handler/Sample.pm
text = '''
# This template file is in the Public Domain.
# You may do anything you want with this file.
'''

package = perl-Math-Int64
location = https://github.com/salva/p5-Math-Int64/blob/master/templates/c_api_h.temp#L2
text = '''
* <% $c_api_h_filename %> - This file is in the public domain
'''

package = perl-Net-OpenID-Consumer
          perl-Net-OpenID-Server
location = https://metacpan.org/release/WROG/Net-OpenID-Consumer-1.18/source/examples/consumer.cgi
           https://metacpan.org/release/ROBN/Net-OpenID-Server-1.09/source/examples/server.cgi
text = '''
This program is in the public domain.
'''

package = perl-perlfaq
location = https://metacpan.org/release/ETHER/perlfaq-5.20210520/source/lib/perlfaq.pod
text = '''
Code examples in all the perlfaq documents are in the public domain.
'''

package = jo
location = https://github.com/jpmens/jo/blob/2cc476178198774ad6bea89be0b9e4ed2d4bf4c5/base64.c#L3
text = '''
This code is public domain software.
'''

package = abseil-cpp
location = https://github.com/abseil/abseil-cpp/blob/c8a2f92586fe9b4e1aff049108f5db8064924d8e/absl/time/internal/cctz/src/tzfile.h#L8
text = '''
This file is in the public domain, so clarified as of
1996-06-05 by Arthur David Olson.
'''

package = ecl
location = https://gitlab.com/embeddable-common-lisp/ecl/-/blob/develop/src/lsp/cmuutil.lsp
text = '''
This code was written as part of the CMU Common Lisp project at
Carnegie Mellon University, and has been placed in the public domain.
'''

package = fontconfig
location = https://gitlab.freedesktop.org/fontconfig/fontconfig/-/blob/main/src/fcmd5.h
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = gap-pkg-profiling
location = https://github.com/gap-packages/profiling/blob/master/src/md5.cc
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = icu4j
location = https://github.com/unicode-org/icu/blob/main/icu4j/main/shared/licenses/LICENSE
text = '''
  ICU uses the public domain data and code derived from Time Zone
Database for its time zone support. The ownership of the TZ database
is explained in BCP 175: Procedure for Maintaining the Time Zone
Database section 7.
7.  Database Ownership
    The TZ database itself is not an IETF Contribution or an IETF
    document.  Rather it is a pre-existing and regularly updated work
    that is in the public domain, and is intended to remain in the
    public domain.  Therefore, BCPs 78 [RFC5378] and 79 [RFC3979] do
    not apply to the TZ Database or contributions that individuals make
    to it.  Should any claims be made and substantiated against the TZ
    Database, the organization that is providing the IANA
    Considerations defined in this RFC, under the memorandum of
    understanding with the IETF, currently ICANN, may act in accordance
    with all competent court orders.  No ownership claims will be made
    by ICANN or the IETF Trust on the database or the code.  Any person
    making a contribution to the database or code waives all rights to
    future claims in that contribution or in the TZ Database.
'''

package = jisksp16-1990-fonts
location = http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/ftp/fonts/jisksp16-1990.bdf.Z
text = '''
COPYRIGHT "Public Domain"
'''

package = pl
location = https://github.com/SWI-Prolog/packages-http/blob/master/examples/calc.pl
text = '''
Copyright (C): Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-semweb/blob/master/murmur.c
text = '''
License: Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-stomp/blob/master/examples/ping.pl
text = '''
@license This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-xpce/blob/master/src/gnu/getdate-source.y
text = '''
This code is in the public domain and has no copyright.
'''

package = pl
          python-pdfminer
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/scripts/swipl-bt
           https://github.com/SWI-Prolog/swipl-devel/blob/master/src/tools/functions.pm
           https://github.com/pdfminer/pdfminer.six/blob/ebf7bcdb983f36d0ff5b40e4f23b52525cb28f18/pdfminer/ascii85.py#L3
text = '''
This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/src/pl-hash.c
text = '''
License: Public domain
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/md5.lisp
text = '''
This file implements The MD5 Message-Digest Algorithm, as defined in
RFC 1321 by R. Rivest, published April 1992.
It was written by Pierre R. Mai, with copious input from the
cmucl-help mailing-list hosted at cons.org, in November 2001 and
has been placed into the public domain.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/metering.lisp
text = '''
This code is in the public domain and is distributed without warranty
of any kind.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/Field/decimals.lisp
text = '''
License: Public domain
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
'''

package = imagej
location = https://imagej.nih.gov/ij/disclaimer.html
text = '''
ImageJ was developed at the National Institutes of Health by an employee of the 
Federal Government in the course of his official duties. Pursuant to Title 17, 
Section 105 of the United States Code, this software is not subject to copyright 
protection and is in the public domain. ImageJ is an experimental system. NIH 
assumes no responsibility whatsoever for its use by other parties, and makes no 
guarantees, expressed or implied, about its quality, reliability, or any other characteristic.
'''

package = gdouros-aegean-fonts
        gdouros-aegyptus-fonts
        gdouros-akkadian-fonts
        gdouros-alexander-fonts
        gdouros-anaktoria-fonts
        gdouros-analecta-fonts
        gdouros-aroania-fonts
        gdouros-asea-fonts
        gdouros-avdira-fonts
        gdouros-musica-fonts
        gdouros-symbola-fonts
location = https://web.archive.org/web/20150625020428/http://users.teilar.gr/~g1951d/
text = '''
in lieu of a licence
Fonts and documents in this site are not pieces of property or merchandise items; they carry no trademark, copyright, license or other market tags; they are free for any use.
'''


package = perl-IO-HTML
location = https://metacpan.org/release/CJM/IO-HTML-1.004/source/examples/detect-encoding.pl
text = '''
This example is hereby placed in the public domain.
You may copy from it freely.

Detect the encoding of files given on the command line
'''

package = dotnet7.0
location = https://github.com/dotnet/fsharp/blob/186d3f6ce267c27f8acaa2c8f3df1472bf2a88bf/src/FSharp.Core/resumable.fs#L2-L3
text = '''
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this software to the public domain worldwide. This software is distributed without any warranty.
'''

package = dotnet7.0
location = https://github.com/dotnet/runtime/blob/e721ae953c61297aec73e8051696f2b9b30148c7/src/coreclr/inc/utilcode.h#L2417-L2425
text = '''
You can use this free for any purpose.  It's in the public domain.  It has no warranty.
'''

package = x11vnc
location = https://github.com/LibVNC/x11vnc/tree/af63109a17f1b1ec8b1e332d215501f11c4a33a0/misc/turbovnc
text = '''
This work has been (or is hereby) released into the public domain by
its author, Karl J. Runge <runge@karlrunge.com>. This applies worldwide.
In case this is not legally possible: Karl J. Runge grants anyone the
right to use this work for any purpose, without any conditions, unless
such conditions are required by law.
'''

package = xmlpull
location = https://github.com/xmlpull-xpp3/xmlpull-xpp3/blob/master/xmlpull/LICENSE.txt
text = '''
All of the XMLPULL API source code, compiled code, and documentation 
contained in this distribution *except* for tests (see separate LICENSE_TESTS.txt)
are in the Public Domain.
XMLPULL API comes with NO WARRANTY or guarantee of fitness for any purpose.
'''

package = xz.java
location = https://git.tukaani.org/?p=xz-java.git;a=blob;f=COPYING;h=8dd17645c4610c3d5eed9bcdd2699ecfac00406b;hb=HEAD
text = ''' 
Licensing of XZ for Java
========================
All the files in this package have been written by Lasse Collin,
Igor Pavlov, and/or Brett Okken. All these files have been put into
the public domain. You can do whatever you want with these files.
This software is provided "as is", without any warranty.
'''

package = libselinux
location = https://github.com/SELinuxProject/selinux/blob/master/libselinux/LICENSE
text = '''
This library (libselinux) is public domain software, i.e. not copyrighted.

Warranty Exclusion
------------------
You agree that this software is a
non-commercially developed program that may contain "bugs" (as that
term is used in the industry) and that it may not function as intended.
The software is licensed "as is". NSA makes no, and hereby expressly
disclaims all, warranties, express, implied, statutory, or otherwise
with respect to the software, including noninfringement and the implied
warranties of merchantability and fitness for a particular purpose.

Limitation of Liability
-----------------------
In no event will NSA be liable for any damages, including loss of data,
lost profits, cost of cover, or other special, incidental,
consequential, direct or indirect damages arising from the software or
the use thereof, however caused and on any theory of liability. This
limitation will apply even if NSA has been advised of the possibility
of such damage. You acknowledge that this is a reasonable allocation of
risk.
'''

package = dnsmasq
location = https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob_plain;f=po/es.po;hb=HEAD
text = '''
This file is put in the public domain.
'''

package = groff
location = http://git.savannah.gnu.org/gitweb/?p=groff.git;a=blob;f=LICENSES;hb=HEAD#l52
text = '''
The files contain no AT&T code and are in the public domain.
'''

package = tzdata
location = https://github.com/eggert/tz/blob/main/zone.tab
           https://github.com/eggert/tz/blob/main/backward
           https://github.com/eggert/tz/blob/main/northamerica
           https://github.com/eggert/tz/blob/main/iso3166.tab
           https://github.com/eggert/tz/blob/main/factory
           https://github.com/eggert/tz/blob/main/europe
           https://github.com/eggert/tz/blob/main/etcetera
           https://github.com/eggert/tz/blob/main/australasia
           https://github.com/eggert/tz/blob/main/asia
           https://github.com/eggert/tz/blob/main/antarctica
           https://github.com/eggert/tz/blob/main/africa
text = '''
# This file is in the public domain, so clarified as of
# 2009-05-17 by Arthur David Olson.
'''

package = tzdata
          abseil-cpp
location = https://github.com/eggert/tz/blob/main/zone1970.tab
           https://github.com/eggert/tz/blob/main/backzone
           https://github.com/abseil/abseil-cpp/blob/c8a2f92586fe9b4e1aff049108f5db8064924d8e/absl/time/internal/cctz/testdata/zoneinfo/zone1970.tab#L3
text = '''
# This file is in the public domain.
'''
